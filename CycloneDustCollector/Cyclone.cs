﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CycloneDustCollector
{
    public class Cyclone
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public double SpeedOptimal { get; set; }

        public double MediumDustSize { get; set; }

        public double DustDispertion { get; set; }
    }
}
