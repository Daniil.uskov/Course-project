﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CycloneDustCollector.Report_Information;

namespace CycloneDustCollector
{
    public class Category
    {
        private string FName;
        private string FDiscr;
        private List<Count> FCount;

        public string Name
        {
            get { return FName; }
        }

        public string Discr
        {
            get { return FDiscr; }
        }

        public List<Count> Counts
        {
            get { return FCount; }
        }

        public Category(string name, string disrk)
        {
            FName = name;
            FDiscr = disrk;
            FCount = new List<Count>();
        }
    }
}
