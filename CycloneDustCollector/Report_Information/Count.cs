﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CycloneDustCollector.Report_Information
{
    public class Count
    {
        public string Name { get; set; }

        public double UnitCount { get; set; }
        public Count(string name, double unitCount)
        {
            Name = name;
            UnitCount = unitCount;
        }

        public Count()
        {

        }
    }
}
