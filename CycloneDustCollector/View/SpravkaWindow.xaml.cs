﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CycloneDustCollector.View
{
    public partial class SpravkaWindow : Window
    {
        public SpravkaWindow()
        {
            InitializeComponent();
        }
        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Hide();
                MainWindow mainWindow = new MainWindow();
                var dialogResult = mainWindow.ShowDialog();
            }
            catch (Exception)
            {
                this.Close();
            }
        }

        private void Image_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var dir = GetExeDirectory();
            var uri = new Uri(System.IO.Path.Combine(dir, "Metodik.pdf"));
            webViewer.Navigate(uri);
        }
        static private string GetExeDirectory()
        {
            string codeBase = Assembly.GetExecutingAssembly().Location;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            path = System.IO.Path.GetDirectoryName(path);
            return path;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var dir = GetExeDirectory();
            var uri = new Uri(System.IO.Path.Combine(dir, "RukovodstvoForUser.pdf"));
            webViewer.Navigate(uri);
        }
    }
}
