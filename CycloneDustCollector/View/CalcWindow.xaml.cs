﻿using CycloneDustCollector.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Library.Formulas;
using CycloneDustCollector.View;

namespace CycloneDustCollector
{
    public partial class CalcWindow : Window
    {
        public CalcWindow()
        {
            InitializeComponent();
            DataContext = new CalcViewModel();
        }
        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void Image_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            SpravkaWindow spravkaWindow = new SpravkaWindow();
            spravkaWindow.ShowDialog();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Image_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            this.Close();

            var calcWindow = new MainWindow();
            calcWindow.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Calculations.Init = ((CalcViewModel)DataContext).GetInitialData();
                var VM = new ResultViewModel();

                VM.GeneralGasPurificationFirst = Calculations.GeneralGasPurificationFirst();
                VM.MinimumDustSize= Calculations.MinimumDustSize();
                VM.ResidualDustContentGasFirst = Calculations.ResidualDustContentGasFirst();
                VM.Coefficient = Calculations.Coefficient();
                VM.CycloneGasVilosity = Calculations.CycloneGasVilosity();
                VM.DryGasVolumatricFlowWork = Calculations.DryGasVolumatricFlowWork();
                VM.InnerDiameterSingleCyclone = Calculations.InnerDiameterSingleCyclone();

                this.Hide();
                ResultWindow resWindow = new ResultWindow();
                resWindow.DataContext = VM;
                var dialogResult = resWindow.ShowDialog();


                this.Show();
            }
            catch (Exception)
            {
                this.Close();
            }
        }
    }
}
