﻿using CycloneDustCollector.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Library.Formulas;

namespace CycloneDustCollector
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent(); 
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
            //this.Close();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Image_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            SpravkaWindow spravkaWindow = new SpravkaWindow();
            spravkaWindow.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Hide();
                CalcWindow calcWindow = new CalcWindow();
                var dialogResult = calcWindow.ShowDialog();
            }
            catch (Exception)
            {
                this.Close();
            }
        }
        private void Label_MouseEnter_1(object sender, MouseEventArgs e)
        {

        }

        private void Label_MouseLeave_1(object sender, MouseEventArgs e)
        {

        }
        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {
            Vihlop.Visibility = Visibility.Visible;
        }

        private void Border_MouseLeave(object sender, MouseEventArgs e)
        {
            Vihlop.Visibility = Visibility.Hidden;
        }

        private void Truba1_MouseEnter(object sender, MouseEventArgs e)
        {
            Truba.Visibility = Visibility.Visible;
        }

        private void Truba1_MouseLeave(object sender, MouseEventArgs e)
        {
            Truba.Visibility = Visibility.Hidden;
        }

        private void Vxod1_MouseEnter(object sender, MouseEventArgs e)
        {
            Vxod.Visibility = Visibility.Visible;
        }

        private void Vxod1_MouseLeave(object sender, MouseEventArgs e)
        {
            Vxod.Visibility = Visibility.Hidden;
        }

        private void Ulitka1_MouseEnter(object sender, MouseEventArgs e)
        {
            Ulitka.Visibility = Visibility.Visible;
        }

        private void Ulitka1_MouseLeave(object sender, MouseEventArgs e)
        {
            Ulitka.Visibility = Visibility.Hidden;
        }

        private void Ciclones1_MouseEnter(object sender, MouseEventArgs e)
        {
            Ciclones.Visibility = Visibility.Visible;
        }


        private void Reshetka1_MouseEnter(object sender, MouseEventArgs e)
        {
            Reshetka.Visibility = Visibility.Visible;
        }

        private void Ciclones1_MouseLeave(object sender, MouseEventArgs e)
        {
            Ciclones.Visibility = Visibility.Hidden;
        }

        private void Reshetka1_MouseLeave(object sender, MouseEventArgs e)
        {
            Reshetka.Visibility = Visibility.Hidden;
        }

        private void Korpus1_MouseEnter(object sender, MouseEventArgs e)
        {
            Korpus.Visibility = Visibility.Visible;
        }

        private void Korpus1_MouseLeave(object sender, MouseEventArgs e)
        {
            Korpus.Visibility = Visibility.Hidden;
        }

        private void Luk_Laz1_MouseEnter(object sender, MouseEventArgs e)
        {
            Luk_Laz.Visibility = Visibility.Visible;
        }

        private void Luk_Laz1_MouseLeave(object sender, MouseEventArgs e)
        {
            Luk_Laz.Visibility = Visibility.Hidden;
        }

        private void DrenajShtucer1_MouseEnter(object sender, MouseEventArgs e)
        {
            DrenajShtucer.Visibility = Visibility.Visible;
        }

        private void DrenajShtucer1_MouseLeave(object sender, MouseEventArgs e)
        {
            DrenajShtucer.Visibility = Visibility.Hidden;
        }

        private void Shtuceri1_MouseEnter(object sender, MouseEventArgs e)
        {
            Shtuceri.Visibility = Visibility.Visible;
        }

        private void Shtuceri1_MouseLeave(object sender, MouseEventArgs e)
        {
            Shtuceri.Visibility = Visibility.Hidden;
        }
    }
}
