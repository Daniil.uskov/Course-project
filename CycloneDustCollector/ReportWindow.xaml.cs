﻿using CycloneDustCollector.Report_Information;
using FastReport;
using FastReport.Export.Image;
using FastReport.Export.PdfSimple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Library.Formulas;

namespace CycloneDustCollector
{
    public partial class ReportWindow : Window
    {
        private List<Category> Categories;

        private List<Count> Counts;

        private string inFolder = @"..\..\..\in\";
        public ReportWindow()
        {
            InitializeComponent();
            inFolder = Utils.FindDirectory("in");
        }

        private void Image_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateBusinessObject();

            Report report = new Report();

            report.Load($@"{inFolder}\report.frx");

            report.RegisterData(Categories, "Categories");
            report.RegisterData(Counts, "Counts");

            report.Prepare();

            report.SavePrepared("Prepared_Report.fpx");

            ImageExport image = new ImageExport();
            image.ImageFormat = ImageExportFormat.Jpeg;
            report.Export(image, "report.jpg");

            #region -- Export to PDF

            PDFSimpleExport pdfExport = new PDFSimpleExport();

            pdfExport.Export(report, "report.pdf");

            #endregion


            report.Dispose();

            buttonPdf.IsEnabled = true;
            buttonPicture.IsEnabled = true;

        }
        private void CreateBusinessObject()
        {
            Counts = new List<Count>()
            {
                new Count { Name = "Общая степень очистки", UnitCount = Calculations.GeneralGasPurificationFirst()}
            };

            Categories = new List<Category>();

            Category category = new Category("Исходные данные", "Данные введенные пользователем");
            category.Counts.Add(new Count("Дисперсия", Calculations.Init.DustDispertion));
            category.Counts.Add(new Count("Скорость", Calculations.Init.SpeedOptimal));
            category.Counts.Add(new Count("Средний размер частиц пыли", Calculations.Init.MediumDustSize));
            category.Counts.Add(new Count("Объемный расход сухого газа, м3/с", Calculations.Init.DryGasVolumatricFlow));
            category.Counts.Add(new Count("Влажность газа, кг/м3", Calculations.Init.MoistureCleaningGas));
            category.Counts.Add(new Count("Избыточное давление, кПа", Calculations.Init.OverPressure));
            category.Counts.Add(new Count("Требуемая степень очистки", Calculations.Init.TheoreticalDegreePurification));
            category.Counts.Add(new Count("Барометрическое давление, кПа", Calculations.Init.BarometricPressure));
            category.Counts.Add(new Count("Температура очистки газа, °C", Calculations.Init.CleaningGasTemp));
            category.Counts.Add(new Count("Концентрация пыли в газе, г/м3", Calculations.Init.DustConcentration));
            category.Counts.Add(new Count("Плотность пылевых частиц, кг/м3", Calculations.Init.DustDensity));
            Categories.Add(category);

            category = new Category("Расчёты", "Данные, полученные по итогам работы программы");
            category.Counts.Add(new Count("Расход газа при н.у., м^3/c", Math.Round(Calculations.DryGasVolumatricFlowWork(), 2)));
            category.Counts.Add(new Count("Диаметр циклона, м", Math.Round(Calculations.InnerDiameterSingleCyclone(), 2)));
            category.Counts.Add(new Count("Расчётная скорость газа, м/с", Math.Round(Calculations.CycloneGasVilosity(), 2)));
            category.Counts.Add(new Count("Коэффициент Кр", Math.Round(Calculations.Coefficient(), 2)));
            category.Counts.Add(new Count("Мин. диаметр частиц, мкм", Math.Round(Calculations.MinimumDustSize(), 2)));
            category.Counts.Add(new Count("Общая степень очистки", Math.Round(Calculations.GeneralGasPurificationFirst(), 2)));
            category.Counts.Add(new Count("Остаточная запылённость газа, г/м^3", Math.Round(Calculations.ResidualDustContentGasFirst(), 2)));
            Categories.Add(category);

        }

        private void buttonPicture_Click(object sender, RoutedEventArgs e)
        {
            var uriImageSource = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"report.jpg", UriKind.RelativeOrAbsolute);
            imagePreview.Source = new BitmapImage(uriImageSource);
        }

        private void buttonPdf_Click(object sender, RoutedEventArgs e)
        {
            var uri = new Uri(AppDomain.CurrentDomain.BaseDirectory + "report.pdf");
            webViewer.Source = uri;
        }
    }
}
