﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Library.Formulas;

namespace CycloneDustCollector.ViewModel
{
    class ResultViewModel: INotifyPropertyChanged
    {
        public double GeneralGasPurificationFirst { get; set; } = Calculations.GeneralGasPurificationFirst();
        public double MinimumDustSize { get; set; } = Calculations.MinimumDustSize();
        public double ResidualDustContentGasFirst { get; set; } = Calculations.ResidualDustContentGasFirst();
        public double Coefficient { get; set; } = Calculations.Coefficient();
        public double CycloneGasVilosity { get; set; } = Calculations.CycloneGasVilosity();
        public double InnerDiameterSingleCyclone { get; set; } = Calculations.InnerDiameterSingleCyclone();
        public double DryGasVolumatricFlowWork { get; set; } = Calculations.DryGasVolumatricFlowWork();

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
