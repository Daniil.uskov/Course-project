﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using Library.Formulas;

namespace CycloneDustCollector.ViewModel
{
    class CalcViewModel : INotifyPropertyChanged
    {
        private readonly CollectionView _cyclonesEntries;

        private int _cycloneEntry = 1;

        public CalcViewModel()
        {
            _BarometricPressure = InitialData.GetDefaultData().BarometricPressure;
            _CleaningGasTemp = InitialData.GetDefaultData().CleaningGasTemp;
            _MoistureCleaningGas = InitialData.GetDefaultData().MoistureCleaningGas;
            _OverPressure = InitialData.GetDefaultData().OverPressure;
            _DryGasVolumatricFlow = InitialData.GetDefaultData().DryGasVolumatricFlow;
            _TheoreticalDegreePurification = InitialData.GetDefaultData().TheoreticalDegreePurification;
            _DustConcentration = InitialData.GetDefaultData().DustConcentration;
            _DustDensity = InitialData.GetDefaultData().DustDensity;

            _cyclonesEntries = new CollectionView(_cyclones);

            var cyclone = _cyclones.FirstOrDefault();
            UpdateInnerPropery(cyclone);
        }
        private void UpdateInnerPropery(Cyclone cyclone)
        {
            _speedOptimal = cyclone.SpeedOptimal;
            _DustDispertion = cyclone.DustDispertion;
            _MediumDustSize = cyclone.MediumDustSize;
            _imageCyclone = "/Image/" + cyclone.Image;
        }

        private IList<Cyclone> _cyclones = new List<Cyclone>()
        {
            new Cyclone
            {
                Id = 1, 
                Name = "ЛИОТ",
                Image = "liot.jpg",
                SpeedOptimal = 3.5,
                MediumDustSize = 3.7, 
                DustDispertion = 0.352
            },
            new Cyclone
            {
                Id = 2,
                Name = "СИОТ",
                Image = "siot.gif",
                SpeedOptimal = 1,
                MediumDustSize = 2.6,
                DustDispertion = 0.28
            },
             new Cyclone
            {
                Id = 3,
                Name = "ВЦНИИОТ",
                Image = "bc.gif",
                SpeedOptimal = 4,
                MediumDustSize = 8.6,
                DustDispertion = 0.32
            },
              new Cyclone
            {
                Id = 4,
                Name = "ЦН-11",
                Image = "cn.jpg",
                SpeedOptimal = 3.5,
                MediumDustSize = 3.65,
                DustDispertion = 0.352
            },
              new Cyclone
            {
                Id = 5,
                Name = "ЦН-15",
                Image = "cn.jpg",
                SpeedOptimal = 3.5,
                MediumDustSize = 4.5,
                DustDispertion = 0.352
            },
              new Cyclone
            {
                Id = 6,
                Name = "ЦН-15У",
                Image = "cn.jpg",
                SpeedOptimal = 3.5,
                MediumDustSize = 6,
                DustDispertion = 0.283
            },
              new Cyclone
            {
                Id = 7,
                Name = "ЦН-24",
                Image = "cn.jpg",
                SpeedOptimal = 4.5,
                MediumDustSize = 8.5,
                DustDispertion = 0.309
            },
              new Cyclone
            {
                Id = 8,
                Name = "СКЦН-34",
                Image = "skcn.gif",
                SpeedOptimal = 1.7,
                MediumDustSize = 1.94,
                DustDispertion = 0.308
            },
              new Cyclone
            {
                Id = 9,
                Name = "СКЦН-34М",
                Image = "skcn.gif",
                SpeedOptimal = 2,
                MediumDustSize = 1.13,
                DustDispertion = 0.340
            },
              new Cyclone
            {
                Id = 10,
                Name = "СДКЦН-33",
                Image = "skcn.gif",
                SpeedOptimal = 2,
                MediumDustSize = 2.31,
                DustDispertion = 0.364
            },
              new Cyclone
            {
                Id = 11,
                Name = "СЦН-40",
                Image = "scn.gif",
                SpeedOptimal = 1.3,
                MediumDustSize = 1.13,
                DustDispertion = 0.34
            },
        };
        public InitialData GetInitialData()
        {
            var initData = InitialData.GetDefaultData();

            //initData.GasCompositionSO2 = SO2;
            //initData.GasCompositionSO2 = SO2;
            //initData.GasCompositionCO = CO;
            initData.SpeedOptimal = SpeedOptimal;
            initData.MediumDustSize = MediumDustSize;
            initData.DustDispertion = DustDispertion;
            initData.DryGasVolumatricFlow = DryGasVolumatricFlow;
            initData.MoistureCleaningGas = MoistureCleaningGas;
            initData.CleaningGasTemp = CleaningGasTemp;
            initData.OverPressure = OverPressure;
            initData.BarometricPressure = BarometricPressure;
            //initData.GasCompositionCO2 = CO2;
            //initData.GasCompositionO2 = O2;
            //initData.GasCompositionN2 = N2;
            //initData.GasCompositionNO2 = NO2;
            //initData.GasCompositionH2S = H2S;
            initData.DustConcentration = DustConcentration;
            initData.DustFractionNumber = 7;
            initData.DustDensity = DustDensity;
            initData.TheoreticalDegreePurification = TheoreticalDegreePurification;

            return initData;
        }
        public CollectionView CycloneEntries
        {
            get { return _cyclonesEntries; }
        }

        public int CycloneEntry
        {
            get { return _cycloneEntry; }
            set
            {
                if (_cycloneEntry == value) return;
                _cycloneEntry = value;

                var cyclone = _cyclones.FirstOrDefault(x => x.Id == _cycloneEntry);

                UpdateInnerPropery(cyclone);

                OnPropertyChanged(nameof(ImageCyclone));
                OnPropertyChanged(nameof(CycloneEntry));
                OnPropertyChanged(nameof(SpeedOptimal));
                OnPropertyChanged(nameof(MediumDustSize));
                OnPropertyChanged(nameof(DustDispertion));
            }
        }

        private double _CO;
        public double CO
        {
            get { return _CO; }
            set
            {
                _CO = InitialData.GetDefaultData().GasCompositionCO;
                OnPropertyChanged(nameof(CO));
            }
        }
        private double _CO2;
        public double CO2
        {
            get { return _CO2; }
            set
            {
                _CO2 = InitialData.GetDefaultData().GasCompositionCO2;

                OnPropertyChanged(nameof(CO2));
            }
        }
        private double _O2;
        public double O2
        {
            get { return _O2; }
            set
            {
                _O2 = InitialData.GetDefaultData().GasCompositionO2;

                OnPropertyChanged(nameof(O2));
            }
        }
        private double _N2;
        public double N2
        {
            get { return _N2; }
            set
            {
                _N2 = InitialData.GetDefaultData().GasCompositionN2;

                OnPropertyChanged(nameof(N2));
            }
        }
        private double _NO2;
        public double NO2
        {
            get { return _NO2; }
            set
            {
                _NO2 = InitialData.GetDefaultData().GasCompositionNO2;

                OnPropertyChanged(nameof(NO2));
            }
        }
        private double _SO2;
        public double SO2
        {
            get { return _SO2; }
            set
            {
                _SO2 = InitialData.GetDefaultData().GasCompositionSO2;

                OnPropertyChanged(nameof(SO2));
            }
        }
        private double _H2S;
        public double H2S
        {
            get { return _H2S; }
            set
            {
                _H2S = InitialData.GetDefaultData().GasCompositionH2S;

                OnPropertyChanged(nameof(H2S));
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _imageCyclone;
        public string ImageCyclone
        {
            get { return _imageCyclone; }
            set
            {
                _imageCyclone = value;
                OnPropertyChanged(nameof(ImageCyclone));
            }
        }


        private double _speedOptimal;
        public double SpeedOptimal { 
            get { return _speedOptimal; } 
            set { 
                _speedOptimal = value;
                OnPropertyChanged(nameof(SpeedOptimal));
            } 
        }

        private double _DustDispertion;
        public double DustDispertion
        {
            get { return _DustDispertion;  }
            set
            {
                _DustDispertion = value;
                OnPropertyChanged(nameof(DustDispertion));
            }
        }

        private double _MediumDustSize;
        public double MediumDustSize
        {
            get { return _MediumDustSize; }
            set
            {
                _MediumDustSize = value;
                OnPropertyChanged(nameof(MediumDustSize));
            }
        }
        private double _DryGasVolumatricFlow;
        public double DryGasVolumatricFlow
        {
            get
            {
                return _DryGasVolumatricFlow;
            }
            set
            {
                _DryGasVolumatricFlow = value;
                OnPropertyChanged(nameof(DryGasVolumatricFlow));
            }
        }

        private double _MoistureCleaningGas;
        public double MoistureCleaningGas
        {
            get
            {
                return _MoistureCleaningGas;
            }
            set
            {
                _MoistureCleaningGas = value;
                OnPropertyChanged(nameof(MoistureCleaningGas));
            }
        }
        private double _OverPressure;
        public double OverPressure
        {
            get
            {
                return _OverPressure;
            }
            set
            {
                _OverPressure = value;
                OnPropertyChanged(nameof(OverPressure));
            }
        }
        private double _BarometricPressure;
        public double BarometricPressure
        {
            get
            {
                return _BarometricPressure;
            }
            set
            {
                _BarometricPressure = value;
                OnPropertyChanged(nameof(BarometricPressure));
            }
        }
        private double _TheoreticalDegreePurification;
        public double TheoreticalDegreePurification
        {
            get
            {
                return _TheoreticalDegreePurification;
            }
            set
            {
                _TheoreticalDegreePurification = value;
                OnPropertyChanged(nameof(TheoreticalDegreePurification));
            }
        }
        private double _CleaningGasTemp;
        public double CleaningGasTemp
        {
            get
            {
                return _CleaningGasTemp;
            }
            set
            {
                _CleaningGasTemp = value;
                OnPropertyChanged(nameof(CleaningGasTemp));
            }
        }
        private double _DustConcentration;
        public double DustConcentration
        {
            get
            {
                return _DustConcentration;
            }
            set
            {
                _DustConcentration = value;
                OnPropertyChanged(nameof(DustConcentration));
            }
        }
        private double _DustDensity;
        public double DustDensity
        {
            get
            {
                return _DustDensity;
            }
            set
            {
                _DustDensity = value;
                OnPropertyChanged(nameof(DustDensity));
            }
        }
        private RelayCommand _CheckerCommand;
        public RelayCommand CheckerCommand
        {
            get
            {
                return _CheckerCommand ??
                (_CheckerCommand = new RelayCommand(obj =>
                {
                    

                },
                (obj) => (BarometricPressure != 0) && (OverPressure != 0) && (DustConcentration != 0) && (CleaningGasTemp != 0) && (MoistureCleaningGas != 0) && (TheoreticalDegreePurification != 0) && (DryGasVolumatricFlow != 0) && (DustDensity != 0)
              ));
            }
        }
    }
}
