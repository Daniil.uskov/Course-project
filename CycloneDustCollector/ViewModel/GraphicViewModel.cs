﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Series;
using Library.Formulas;
using OxyPlot.Axes;
using System.Windows.Markup;
using System.Windows.Data;
using System.Globalization;
using System.Windows;

namespace CycloneDustCollector.ViewModel
{
    public class GraphicViewModel: INotifyPropertyChanged
    {
        public GraphicViewModel()
        {
            _Ox = 200;
            _Oy = 2;

            this.Title = "Зависимость фракционной степени очистки газа от интеграла вероятности функции распределения";
            this.Points = new List<DataPoint>
            {
                new DataPoint(FractionalDegreeGasPurificationFirst[0], IntDistributionFunctionAll[0]),
                new DataPoint(FractionalDegreeGasPurificationFirst[1], IntDistributionFunctionAll[1]),
                new DataPoint(FractionalDegreeGasPurificationFirst[2], IntDistributionFunctionAll[2]),
                new DataPoint(FractionalDegreeGasPurificationFirst[3], IntDistributionFunctionAll[3]),
                new DataPoint(FractionalDegreeGasPurificationFirst[4], IntDistributionFunctionAll[4]),
                new DataPoint(FractionalDegreeGasPurificationFirst[5], IntDistributionFunctionAll[5]),
                new DataPoint(FractionalDegreeGasPurificationFirst[6], IntDistributionFunctionAll[6])
            };
        }
        public string Title { get; private set; }
        public IList<DataPoint> Points { get; private set; }

        private double _Ox;

        public double Ox
        {
            get

            {
                return _Ox;
            }
            set
            {
                _Ox = Convert.ToInt32(value);
                OnPropertyChanged(nameof(Ox));
                OnPropertyChanged(nameof(Points));
            }
        }

        private double _Oy;

        public double Oy
        {
            get
            {
                return _Oy;
            }
            set
            {
                _Oy = Convert.ToInt32(value);
                OnPropertyChanged(nameof(Oy));
                OnPropertyChanged(nameof(Points));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public double[] IntDistributionFunctionAll { get; set; } = Calculations.IntDistributionFunctionAll();
        public double[] FractionalDegreeGasPurificationFirst { get; set; } = Calculations.FractionalDegreeGasPurificationFirst();
    }
    
}
