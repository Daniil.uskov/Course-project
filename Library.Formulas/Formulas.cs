﻿using System;

namespace Library.Formulas
{
    public class InitialData
    {
        /// <summary> Оптимальная скорость газа в циклоне ωопт </summary>///
        public double SpeedOptimal { get; set; }
        /// <summary> Средний медианный размер частиц пыли, теоретически улавливаемой на 50%, м </summary>///
        public double MediumDustSize { get; set; }
        /// <summary> Дисперсия размеров пылевых частиц от стандартного распределения lg </summary>///
        public double DustDispertion { get; set; }
        /// <summary> Объемный расход сухого газа при нормальных условиях, м3/с , Vг0 </summary>///
        public double DryGasVolumatricFlow { get; set; }
        /// <summary> Влажность очищаемого газа, кг/м3,  Xг </summary>///
        public double MoistureCleaningGas { get; set; }
        /// <summary> Температура очищаемого газа, tг </summary>///
        public double CleaningGasTemp { get; set; }
        /// <summary> Избыточное давление (+) или разрежение (-) очищаемого газа, кПа, Pг </summary>///
        public double OverPressure { get; set; }
        /// <summary> Барометрическое давление, кПа, Pбар </summary>///
        public double BarometricPressure { get; set; } 
        /// <summary> Состав газа, X(CO) </summary>///
        public double GasCompositionCO { get; set; } 
        /// <summary> Состав газа, X(CO2) </summary>///
        public double GasCompositionCO2 { get; set; }
        /// <summary> Состав газа, X(O2) </summary>///
        public double GasCompositionO2 { get; set; }
        /// <summary> Состав газа, X(N2) </summary>///
        public double GasCompositionN2 { get; set; }
        /// <summary> Состав газа, X(SO2) </summary>///
        public double GasCompositionSO2 { get; set; }
        /// <summary> Состав газа, X(NO2)  </summary>///
        public double GasCompositionNO2 { get; set; }
        /// <summary> Состав газа, X(H2S)г </summary>///
        public double GasCompositionH2S { get; set; }
        /// <summary> Концентрация пыли в очищаемом газе Z, г/м3 </summary>///
        public double DustConcentration { get; set; }
        /// <summary> Число фракций пыли, N </summary>///
        public double DustFractionNumber { get; set; }
        /// <summary> Дисперсный состав пыли, di </summary>///
        public double[] DispersedCompositionDust { get; set; }
        /// <summary> Процентный состав пыли, имеющей размер частицы di и менее Pi, %di<Pi </summary>///
        public double[] DustPresentage { get; set; }
        /// <summary> Влажность очищаемого газа, кг/м3,  Xг </summary>///
        public double DustDensity { get; set; }
        /// <summary> Теоретическая степень очистки газа, %, ηт </summary>///
        public double TheoreticalDegreePurification { get; set; }
        /// <summary> Поправка на отличие диаметра циклона от 500 мм, K1 </summary>///
        public double DiameterDifferenceCorrection { get; set; } 
        /// <summary> Поправка на влияние запыленности газа, K2 </summary>///
        public double DustCorrection { get; set; }
        /// <summary> Поправку на влияние групповой компоновки циклонов, K3 </summary>///
        public double GroupLayoutCorrection { get; set; } 
        /// <summary> Гидравлическое сопротивление циклона </summary>///
        public double CycloneHydralicResistance { get; set; } 
        public double Radious { get; set; } // Радиус, R
        public const double Pi = 3.14; // Постоянная PI
        public const double e = 0.27; // Постоянная e

        /// <summary>
        /// Исходные данные
        /// </summary>
        public static InitialData GetDefaultData()
        {
            return new InitialData
            {
                SpeedOptimal = 3.5,
                MediumDustSize = 0.0000037,
                DustDispertion = 0.352,
                DryGasVolumatricFlow = 16,
                MoistureCleaningGas = 0.013,
                CleaningGasTemp = 130,
                OverPressure = 15,
                BarometricPressure = 101,
                GasCompositionCO = 0,
                GasCompositionCO2 = 0.13,
                GasCompositionO2 = 0,
                GasCompositionN2 = 0.76,
                GasCompositionSO2 = 0,
                GasCompositionNO2 = 0,
                GasCompositionH2S = 0.11,
                DustConcentration = 40,
                DustFractionNumber = 7,
                DispersedCompositionDust = new double[] { 0.0000025, 0.000004, 0.0000063, 0.00001, 0.000016, 0.000025, 0.00004 },
                DustPresentage = new double[] { 1.5, 3, 7, 14, 28, 50, 100 },
                CycloneHydralicResistance = 1050,
                DustDensity = 2150,
                TheoreticalDegreePurification = 86,
                DiameterDifferenceCorrection = 1,
                DustCorrection = 0.93,
                GroupLayoutCorrection = 60,
                Radious = 0.25
            };
        }
    }

    /// <summary>
    /// Расчёты
    /// </summary>
    public class Calculations 
    {
        public static InitialData Init { get; set; } = InitialData.GetDefaultData();

        /// <summary>
        /// Поправочный коэффициент на отличие рабочих условий от нормальных, β
        /// </summary>
        public static double CorrectionFactor()
        {
            double CorrectionFactor = 273 * (Init.BarometricPressure + Init.OverPressure) / (101.3 * (Init.CleaningGasTemp + 273));
            return CorrectionFactor;
        }

        /// <summary>
        /// Плотность сухого газа при нормальных условиях, кг/м3, ρ0
        /// </summary>
        public static double DryGasDansityNormal()
        {
            double DryGasDansityNormal = 1.25 * Init.GasCompositionCO + 1.97 * Init.GasCompositionCO2 + 1.429 * Init.GasCompositionO2 + 1.525 * Init.GasCompositionN2 + 2.927 * Init.GasCompositionSO2 + 2.051 * Init.GasCompositionNO2 + 1.539 * Init.GasCompositionH2S;
            return DryGasDansityNormal;
        }

        /// <summary>
        /// Плотность сухого газа при рабочих условиях, кг/м, ρс
        /// </summary>
        public static double DryGasDansityWork()
        {
            double DryGasDansityWork = DryGasDansityNormal() * CorrectionFactor();
            return DryGasDansityWork;
        }

        /// <summary>
        /// Расход влажного газа при рабочих условиях, V
        /// </summary>
        public static double MoistureGasDansityWork()
        {
            double MoistureGasDansityWork = (DryGasDansityNormal() + Init.MoistureCleaningGas) * CorrectionFactor() / (1 + (Init.MoistureCleaningGas / 0.804));
            return MoistureGasDansityWork;
        }

        /// <summary>
        /// Промежуточный коэффициент, A1
        /// </summary>
        public static double IntermediateCoefficientFirst()
        {
            double IntermediateCoefficientFirst = 273 + Init.CleaningGasTemp;
            return IntermediateCoefficientFirst;
        }

        /// <summary>
        /// Промежуточный коэффициент, A2
        /// </summary>
        public static double IntermediateCoefficientSecond()
        {
            double IntermediateCoefficientSecond = Math.Pow(IntermediateCoefficientFirst() / 273, 1.5);
            return IntermediateCoefficientSecond;
        }

        /// <summary>
        /// Пересчет сухого газа на состав влажного газа, %, K
        /// </summary>
        public static double ConvertionDryGasToWet()
        {
            double ConvertionDryGasToWet = 100 / (100 + (124.4 * Init.MoistureCleaningGas));
            return ConvertionDryGasToWet;
        }

        /// <summary>
        /// X^b(CO)
        /// </summary>
        public static double GasCompositionWetCO()
        {
            double GasCompositionWetCO = Init.GasCompositionCO * ConvertionDryGasToWet();
            return GasCompositionWetCO;
        }

        /// <summary>
        /// X^b(CO2)
        /// </summary>
        public static double GasCompositionWetCO2()
        {
            double GasCompositionWetCO2 = Init.GasCompositionCO2 * ConvertionDryGasToWet();
            return GasCompositionWetCO2;
        }

        /// <summary>
        /// X^b(O2)
        /// </summary>
        public static double GasCompositionWetO2()
        {
            double GasCompositionWetO2 = Init.GasCompositionO2 * ConvertionDryGasToWet();
            return GasCompositionWetO2;
        }

        /// <summary>
        /// X^b(N2)
        /// </summary>
        public static double GasCompositionWetN2()
        {
            double GasCompositionWetN2 = Init.GasCompositionN2 * ConvertionDryGasToWet();
            return GasCompositionWetN2;
        }

        /// <summary>
        /// X^b(NO2)
        /// </summary>
        public static double GasCompositionWetNO2()
        {
            double GasCompositionWetNO2 = Init.GasCompositionNO2 * ConvertionDryGasToWet();
            return GasCompositionWetNO2;
        }

        /// <summary>
        /// X^b(SO2)
        /// </summary>
        public static double GasCompositionWetSO2()
        {
            double GasCompositionWetSO2 = Init.GasCompositionSO2 * ConvertionDryGasToWet();
            return GasCompositionWetSO2;
        }

        /// <summary>
        /// X^b(H2S)
        /// </summary>
        public static double GasCompositionWetH2S()
        {
            double GasCompositionWetH2S = Init.GasCompositionH2S * ConvertionDryGasToWet();
            return GasCompositionWetH2S;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µСО
        /// </summary>
        public static double DynamicViscosityWorkCO()
        {
            double DynamicViscosityWorkCO = 0.0000166 * ((373 / (IntermediateCoefficientFirst() + 100)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkCO;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µСО2
        /// </summary>
        public static double DynamicViscosityWorkCO2()
        {
            double DynamicViscosityWorkCO2 = 0.00001496 * ((528 / (IntermediateCoefficientFirst() + 255)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkCO2;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µО2
        /// </summary>
        public static double DynamicViscosityWorkO2()
        {
            double DynamicViscosityWorkO2 = 0.0000203 * ((404 / (IntermediateCoefficientFirst() + 131)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkO2;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µN2
        /// </summary>
        public static double DynamicViscosityWorkN2()
        {
            double DynamicViscosityWorkN2 = 0.000017 * ((387 / (IntermediateCoefficientFirst() + 114)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkN2;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µSO2
        /// </summary>
        public static double DynamicViscosityWorkSO2()
        {
            double DynamicViscosityWorkSO2 = 0.0000117 * ((669 / (IntermediateCoefficientFirst() + 396)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkSO2;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µNО2
        /// </summary>
        public static double DynamicViscosityWorkNO2()
        {
            double DynamicViscosityWorkNO2 = 0.0000288 * ((464 / (IntermediateCoefficientFirst() + 191)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkNO2;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µH2S
        /// </summary>
        public static double DynamicViscosityWorkH2S()
        {
            double DynamicViscosityWorkH2S = 0.0000116 * ((773 / (IntermediateCoefficientFirst() + 500)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkH2S;
        }

        /// <summary>
        /// Динамическая вязкость компонентов газа при рабочих условиях,  µH2O 
        /// </summary>
        public static double DynamicViscosityWorkH2O()
        {
            double DynamicViscosityWorkH2O = 0.00001 * ((1234 / (IntermediateCoefficientFirst() + 961)) * IntermediateCoefficientSecond());
            return DynamicViscosityWorkH2O;
        }

        /// <summary>
        /// Динамическая вязкость газа при рабочих условиях, µr 
        /// </summary>
        public static double DynamicViscosityGasWork()
        {
            double DynamicViscosityGasWork = ((GasCompositionWetCO() * DynamicViscosityWorkCO()) + (GasCompositionWetCO2() * DynamicViscosityWorkCO2()) + (GasCompositionWetO2() * DynamicViscosityWorkO2()) + (GasCompositionWetN2() * DynamicViscosityWorkN2()) + (GasCompositionWetNO2() * DynamicViscosityWorkNO2()) + (GasCompositionWetSO2() * DynamicViscosityWorkSO2()) + (GasCompositionWetH2S() * DynamicViscosityWorkH2S())) / ((DryGasDansityWork()) * MoistureGasDansityWork());
            return DynamicViscosityGasWork;
        }

        /// <summary>
        /// Расход влажного газа при рабочих условиях, V 
        /// </summary>
        public static double DryGasVolumatricFlowWork()
        {
            double DryGasVolumatricFlowWork = Init.DryGasVolumatricFlow * (CorrectionFactor() * (1 + (Init.MoistureCleaningGas / 0.804)));
            return DryGasVolumatricFlowWork;
        }

        /// <summary>
        /// Общая площадь проходного сечения циклонов, м2, F
        /// </summary>
        public static double CyclonesFlowArea()
        {
            double CyclonesFlowArea = DryGasVolumatricFlowWork() / Init.SpeedOptimal;
            return CyclonesFlowArea;
        }

        /// <summary>
        /// Внутренний расчетный диаметр одиночного циклона, мм, Dц
        /// </summary>
        public static double InnerDiameterSingleCyclone()
        {
            double InnerDiameterSingleCyclone = 1000 * Math.Sqrt(CyclonesFlowArea() / (0.758 * Init.DustFractionNumber));
            return InnerDiameterSingleCyclone;
        }

        /// <summary>
        /// Расчетная скорость газа в циклоне, м/с, ω
        /// </summary>
        public static double CycloneGasVilosity()
        {
            double CycloneGasVilosity = DryGasVolumatricFlowWork() / (0.785 * Init.DustFractionNumber * 0.000001 * InnerDiameterSingleCyclone() * InnerDiameterSingleCyclone());
            return CycloneGasVilosity;
        }

        /// <summary>
        /// Отличие расчетной скорости от оптимальной,  Δω
        /// </summary>
        public static double DiffDisignOptimal()
        {
            double DiffDisignOptimal = (CycloneGasVilosity() - Init.SpeedOptimal) * (100 / Init.SpeedOptimal);
            return DiffDisignOptimal;
        }

        /// <summary>
        /// Гидравлическое сопротивление циклона, Па, ΔP
        /// </summary>
        public static double HydralicResistance()
        {
            double HydralicResistance = Init.CycloneHydralicResistance * MoistureGasDansityWork() * (CycloneGasVilosity() * CycloneGasVilosity() / 2);
            return HydralicResistance;
        }

        /// <summary>
        /// Коэффициент, Kp 
        /// </summary>
        public static double Coefficient()
        {
            double Coefficient = HydralicResistance() / MoistureGasDansityWork();
            return Coefficient;
        }

        /// <summary>
        /// Минимальный размер частиц пыли, которые теоретически могут улавливаться циклоном на 50% при реальных условиях, мкм, dmin
        /// </summary>
        public static double MinimumDustSize()
        {
            double MinimumDustSize = Init.MediumDustSize * Init.Radious * 1000 * Math.Sqrt(0.001 * InnerDiameterSingleCyclone() * (DynamicViscosityGasWork() / (MoistureGasDansityWork() * CycloneGasVilosity())));
            return MinimumDustSize;
        }

        /// <summary>
        /// Аргумент интеграла вероятности функции распределения, Xi 
        /// </summary>
        public static double[] ArgIntegralDistributionFunction()
        {
            double[] ArgIntegralDistributionFunction = new double[(int)Init.DustFractionNumber];
            for (int i = 0; i <= Init.DustFractionNumber - 1; i++)
            {
                ArgIntegralDistributionFunction[i] = Math.Log10(Init.DispersedCompositionDust[i] / MinimumDustSize()) / Init.DustDispertion;
            }
            return ArgIntegralDistributionFunction;
        }

        /// <summary>
        /// Интеграл вероятности функции распределения, Ф(Xi) 
        /// </summary>
        public static double[] IntDistributionFunctionAll()
        {
            double[] IntDistributionFunctionAll = new double[(int)Init.DustFractionNumber];
            IntDistributionFunctionAll[0] = 0.4245;
            IntDistributionFunctionAll[1] = 0.7457;
            IntDistributionFunctionAll[2] = 0.8385;
            IntDistributionFunctionAll[3] = 0.9109;
            for (int i = 4; i <= Init.DustFractionNumber - 1; i++)
            {
                IntDistributionFunctionAll[i] = 1 - (1 / Math.Sqrt(InitialData.Pi) * Math.Pow(InitialData.e, Math.Pow(-1 * ArgIntegralDistributionFunction()[i] / Math.Sqrt(2), 2)) * ((1 / ArgIntegralDistributionFunction()[i])
                    - (1 / (2 * Math.Pow(ArgIntegralDistributionFunction()[i], 3))) + (3 / (4 * Math.Pow(ArgIntegralDistributionFunction()[i], 5)))
                    - (15 / (8 * Math.Pow(ArgIntegralDistributionFunction()[i], 7))) + (105 / (16 * Math.Pow(ArgIntegralDistributionFunction()[i], 9)))
                    - (945 / (32 * Math.Pow(ArgIntegralDistributionFunction()[i], 11))) + (945 * 11 / (64 * Math.Pow(ArgIntegralDistributionFunction()[i], 13)))));
            }
            return IntDistributionFunctionAll;
        }

        /// <summary>
        /// Фракционная степень очистки газа, %, ηi
        /// </summary>
        public static double[] FractionalDegreeGasPurificationFirst()
        {
            double[] FractionalDegreeGasPurificationFirst = new double[(int)Init.DustFractionNumber];
            for (int i = 0; i <= Init.DustFractionNumber - 1; i++)
            {
                FractionalDegreeGasPurificationFirst[i] = 50 * (1 + IntDistributionFunctionAll()[i]);
            }
            return FractionalDegreeGasPurificationFirst;
        }

        /// <summary>
        /// Процентное содержание частиц пыли размером di, %, ΔPi
        /// </summary>
        public static double[] ProcentageDustParticles()
        {
            double[] ProcentageDustParticles = new double[Init.DustPresentage.Length];
            ProcentageDustParticles[0] = 1.5;
            for (int i = 0; i < ProcentageDustParticles.Length - 1; i++)
            {
                ProcentageDustParticles[i + 1] = Init.DustPresentage[i + 1] - Init.DustPresentage[i];
            }
            return ProcentageDustParticles;
        }

        /// <summary>
        /// Общая степень очистки газа, %, η0
        /// </summary>
        public static double GeneralGasPurificationFirst()
        {
            double GeneralGasPurificationFirst = 0;
            for (int i = 0; i <= Init.DustPresentage.Length - 1; i++)
            {
                GeneralGasPurificationFirst += ProcentageDustParticles()[i] * FractionalDegreeGasPurificationFirst()[i];
            }
            GeneralGasPurificationFirst = 0.01 * GeneralGasPurificationFirst;
            return GeneralGasPurificationFirst;
        }

        /// <summary>
        /// Остаточная запыленность газа, г/м3, Zост
        /// </summary>
        public static double ResidualDustContentGasFirst()
        {
            double ResidualDustContentGasFirst = Init.DustConcentration * (1 - 0.01 * GeneralGasPurificationFirst());
            return ResidualDustContentGasFirst;
        }

        /// <summary>
        /// Остаточное содержание в газе отдельных фракций пыли, г/м3, и их массовая доля, %, Z0i
        /// </summary>
        public static double[] ResidualContentDustFractionsGasFirst()
        {
            double[] ResidualContentDustFractionsGasFirst = new double[Init.DustPresentage.Length];
            for (int i = 0; i <= Init.DustPresentage.Length - 1; i++)
            {
                ResidualContentDustFractionsGasFirst[i] = (1 - 0.01 * FractionalDegreeGasPurificationFirst()[i]) * Init.DustConcentration * 0.01 * ProcentageDustParticles()[i];
            }
            return ResidualContentDustFractionsGasFirst;
        }
    }


}
