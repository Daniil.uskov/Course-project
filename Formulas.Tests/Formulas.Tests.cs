﻿using NUnit.Framework;
using Library.Formulas;
using System;

namespace Formulas.Tests
{
    public class Tests
    {

        [Test]
        public void CorrectionFacrotCulcTest() // Тест на поправочный коэффициент на отличие рабочих условий от нормальных, β
        {
            double expected = 0.775722065;

            var actual = Calculations.CorrectionFactor();

            Assert.AreEqual(expected, actual, 0.001);
        }

        [Test]
        public void DryGasDansityNormalTest() // Тест плотность сухого газа при нормальных условиях, кг/м3, ρ0
        {
            double expected = 1.58439;

            var actual = Calculations.DryGasDansityNormal();

            Assert.AreEqual(expected, actual, 0.001);
        }

        [Test]
        public void DryGasDansityWorklTest() // Тест на плотность сухого газа при рабочих условиях, кг/м, ρс
        {
            double expected = 1.229046282;

            var actual = Calculations.DryGasDansityWork();

            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void MoistureGasDansityWorkTest() // Тест на расход влажного газа при рабочих условиях, V
        {
            double expected = 1.21941378;

            var actual = Calculations.MoistureGasDansityWork();

            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void IntermediateCoefficientFirstTest() // Тест на промежуточный коэффициент, A1
        {
            double expected = 403;

            var actual = Calculations.IntermediateCoefficientFirst();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void IntermediateCoefficientSecondTest() // Тест на промежуточный коэффициент, A2
        {
            double expected = 1.793550456;

            var actual = Calculations.IntermediateCoefficientSecond();
            Assert.AreEqual(expected, actual, 0.001);

        }
        [Test]
        public void ConvertionDryGasToWetTest() // Тест на пересчет сухого газа на состав влажного газа, %, K
        {
            double expected = 0.984085371;

            var actual = Calculations.ConvertionDryGasToWet();
            Assert.AreEqual(expected, actual, 0.001);

        }
        [Test]
        public void GasCompositionWetCOTest() // Тест на X^b(CO)
        {
            double expected = 0;

            var actual = Calculations.GasCompositionWetCO();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void GasCompositionWetCO2Test() // Тест на X^b(CO2)
        {
            double expected = 0.127931098;

            var actual = Calculations.GasCompositionWetCO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void GasCompositionWetO2Test() // Тест на X^b(O2)
        {
            double expected = 0;

            var actual = Calculations.GasCompositionWetO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void GasCompositionWetN2Test() // Тест на X^b(N2)
        {
            double expected = 0.747904882;

            var actual = Calculations.GasCompositionWetN2();
            Assert.AreEqual(expected, actual, 0.001);

        }
        [Test]
        public void GasCompositionWetNO2Test() // Тест на X^b(NO2)
        {
            double expected = 0;

            var actual = Calculations.GasCompositionWetNO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void GasCompositionWetSO2Test() // Тест на X^b(SO2)
        {
            double expected = 0;

            var actual = Calculations.GasCompositionWetSO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void GasCompositionWetH2STest() // Тест на X^b(H2S)
        {
            double expected = 0.108249391;

            var actual = Calculations.GasCompositionWetH2S();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkCOTest() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µСО
        {
            double expected = 0.00002208;

            var actual = Calculations.DynamicViscosityWorkCO();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkCO2Test() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µСО2
        {
            double expected = 0.00002153;

            var actual = Calculations.DynamicViscosityWorkCO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkO2Test() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µО2
        {
            double expected = 0.00002755;

            var actual = Calculations.DynamicViscosityWorkO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkN2Test() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µN2
        {
            double expected = 0.00002282;

            var actual = Calculations.DynamicViscosityWorkN2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkS02Test() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µSO2
        {
            double expected = 0.00001757;

            var actual = Calculations.DynamicViscosityWorkSO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkNO2Test() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µNО2
        {
            double expected = 0.00004035;

            var actual = Calculations.DynamicViscosityWorkNO2();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkH2STest() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µH2S
        {
            double expected = 0.00001781;

            var actual = Calculations.DynamicViscosityWorkH2S();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityWorkH2OTest() // Тест на динамическую вязкость компонентов газа при рабочих условиях,  µH2O 
        {
            double expected = 0.00001623;

            var actual = Calculations.DynamicViscosityWorkH2O();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DynamicViscosityGasWorkTest() // Тест на динамическую вязкость газа при рабочих условиях, µr 
        {
            double expected = 0.00001451;

            var actual = Calculations.DynamicViscosityGasWork();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void DryGasVolumatricFlowWorkTest() // Тест на расход влажного газа при рабочих условиях, V 
        {
            double expected = 12.61223735;

            var actual = Calculations.DryGasVolumatricFlowWork();
            Assert.AreEqual(expected, actual, 0.001);

        }
        [Test]
        public void CyclonesFlowAreaTest() // Тест на общую площадь проходного сечения циклонов, м2, F
        {
            double expected = 3.603496386;

            var actual = Calculations.CyclonesFlowArea();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void InnerDiameterSingleCycloneTest() // Тест на внутренний расчетный диаметр одиночного циклона, мм, Dц
        {
            double expected = 824.0971697;

            var actual = Calculations.InnerDiameterSingleCyclone();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void CycloneGasVilosityTest() // Тест на расчетную скорость газа в циклоне, м/с, ω
        {
            double expected = 3.379617834;

            var actual = Calculations.CycloneGasVilosity();
            Assert.AreEqual(expected, actual, 0.001);
        }

        [Test]
        public void DiffDisignOptimalTest() // Тест на отличие расчетной скорости от оптимальной,  Δω
        {
            double expected = -3.439490446;

            var actual = Calculations.DiffDisignOptimal();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void HydralicResistanceTest() // Тест на гидравлическое сопротивление циклона, Па, ΔP
        {
            double expected = 7312.158357;

            var actual = Calculations.HydralicResistance();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void CoefficientTest() // Тест на коэффициент, Kp 
        {
            double expected = 5996.453771;

            var actual = Calculations.Coefficient();
            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void MinimumDustSizeTest() // Тест на минимальный размер частиц пыли, которые теоретически могут улавливаться циклоном на 50% при реальных условиях, мкм, dmin
        {
            double expected = 0.000001576;

            var actual = Calculations.MinimumDustSize();
            Assert.AreEqual(expected, actual, 0.00000001);
        }

        [Test]
        public void ArgIntegralDistributionFunctionTest() // Тест на аргумент интеграла вероятности функции распределения, Xi 
        {
            double[] expected = new double[] { 0.569394319, 1.149280633, 1.709736764, 2.279792021, 2.859678336, 3.41030341, 3.990189724 };
            var actual = Calculations.ArgIntegralDistributionFunction();

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], 0.001);
            }
        }
        [Test]
        public void IntDistributionFunctionAllTest() // Тест на интеграл вероятности функции распределения, Ф(Xi) 
        {
            double[] expected = new double[] { 0.4245, 0.7457, 0.8385, 0.9109, 0.999995814, 0.999999961, 0.999995916 };
            var actual = Calculations.IntDistributionFunctionAll();

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], 0.001);
            }
        }
        [Test]
        public void FractionalDegreeGasPurificationFirstTest() // Тест на фракционную степень очистки газа, %, ηi
        {
            double[] expected = new double[] { 71.225, 87.285, 91.925, 95.545, 99.95575734, 99.99607301, 99.99979582 };

            var actual = Calculations.FractionalDegreeGasPurificationFirst();

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], 0.001);
            }
        }
        [Test]
        public void ProcentageDustParticlesTest() // Тест на процентное содержание частиц пыли размером di, %, ΔPi
        {
            double[] expected = new double[] { 1.5, 1.5, 4, 7, 14, 22, 50 };

            var actual = Calculations.ProcentageDustParticles();

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], 0.001);
            }
        }
        [Test]
        public void GeneralGasPurificationFirstTest() // Тест на общую степень очистки газа, %, η0
        {
            double expected = 98.73564;
            var actual = Calculations.GeneralGasPurificationFirst();

            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void ResidualContentDustFractionsGasFirst() // Тест на остаточную запыленность газа, г/м3, Zост
        {
            double[] expected = new double[] { 0.17265, 0.07629, 0.1292, 0.1247400000, 0.0024775891, 0.0003455752, 0.0000408352 };
            var actual = Calculations.ResidualContentDustFractionsGasFirst();

            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i], 0.001);
            }
        }
        [Test]
        public void ResidualDustContentGasFirstTest() // Тест на остаточное содержание в газе отдельных фракций пыли, г/м3, и их массовая доля, %, Z0i
        {
            double expected = 0.505744;
            var actual = Calculations.ResidualDustContentGasFirst();

            Assert.AreEqual(expected, actual, 0.001);
        }
        [Test]
        public void AllTest() // Тест на все расчёты
        {
            double expectedCorrectionFactor = 0.775722065;
            var actualCorrectionFactor = Calculations.CorrectionFactor();
            Assert.AreEqual(expectedCorrectionFactor, actualCorrectionFactor, 0.001);


            double expectedDryGasDansityNormal = 1.58439;
            var actualDryGasDansityNormal = Calculations.DryGasDansityNormal();
            Assert.AreEqual(expectedDryGasDansityNormal, actualDryGasDansityNormal, 0.001);


            double expectedDryGasDansityWork = 1.229046282;
            var actualDryGasDansityWork = Calculations.DryGasDansityWork();
            Assert.AreEqual(expectedDryGasDansityWork, actualDryGasDansityWork, 0.001);


            double expectedMoistureGasDansityWork = 1.21941378;
            var actualMoistureGasDansityWork = Calculations.MoistureGasDansityWork();
            Assert.AreEqual(expectedMoistureGasDansityWork, actualMoistureGasDansityWork, 0.001);


            double expectedIntermediateCoefficientFirst = 403;
            var actualIntermediateCoefficientFirst = Calculations.IntermediateCoefficientFirst();
            Assert.AreEqual(expectedIntermediateCoefficientFirst, actualIntermediateCoefficientFirst, 0.001);


            double expectedIntermediateCoefficientSecond = 1.793550456;
            var actualIntermediateCoefficientSecond = Calculations.IntermediateCoefficientSecond();
            Assert.AreEqual(expectedIntermediateCoefficientSecond, actualIntermediateCoefficientSecond, 0.001);


            double expectedConvertionDryGasToWet = 0.984085371;
            var actualConvertionDryGasToWet = Calculations.ConvertionDryGasToWet();
            Assert.AreEqual(expectedConvertionDryGasToWet, actualConvertionDryGasToWet, 0.001);


            double expectedGasCompositionWetCO = 0;
            var actualGasCompositionWetCO = Calculations.GasCompositionWetCO();
            Assert.AreEqual(expectedGasCompositionWetCO, actualGasCompositionWetCO, 0.001);


            double expectedGasCompositionWetCO2 = 0.127931098;
            var actualGasCompositionWetCO2 = Calculations.GasCompositionWetCO2();
            Assert.AreEqual(expectedGasCompositionWetCO2, actualGasCompositionWetCO2, 0.001);


            double expectedGasCompositionWetO2 = 0;
            var actualGasCompositionWetO2 = Calculations.GasCompositionWetO2();
            Assert.AreEqual(expectedGasCompositionWetO2, actualGasCompositionWetO2, 0.001);


            double expectedGasCompositionWetN2 = 0.747904882;
            var actualGasCompositionWetN2 = Calculations.GasCompositionWetN2();
            Assert.AreEqual(expectedGasCompositionWetN2, actualGasCompositionWetN2, 0.001);


            double expectedGasCompositionWetNO2 = 0;
            var actualGasCompositionWetNO2 = Calculations.GasCompositionWetNO2();
            Assert.AreEqual(expectedGasCompositionWetNO2, actualGasCompositionWetNO2, 0.001);


            double expectedGasCompositionWetSO2 = 0;
            var actualGasCompositionWetSO2 = Calculations.GasCompositionWetSO2();
            Assert.AreEqual(expectedGasCompositionWetSO2, actualGasCompositionWetSO2, 0.001);


            double expectedGasCompositionWetH2S = 0.108249391;
            var actualGasCompositionWetH2S = Calculations.GasCompositionWetH2S();
            Assert.AreEqual(expectedGasCompositionWetH2S, actualGasCompositionWetH2S, 0.001);


            double expectedDynamicViscosityWorkCO = 0.00002208;
            var actuaDynamicViscosityWorkCO = Calculations.DynamicViscosityWorkCO();
            Assert.AreEqual(expectedDynamicViscosityWorkCO, actuaDynamicViscosityWorkCO, 0.001);


            double expectedDynamicViscosityWorkCO2 = 0.00002153;
            var actualDynamicViscosityWorkCO2 = Calculations.DynamicViscosityWorkCO2();
            Assert.AreEqual(expectedDynamicViscosityWorkCO2, actualDynamicViscosityWorkCO2, 0.001);


            double expectedDynamicViscosityWorkO2 = 0.00002755;
            var actualDynamicViscosityWorkO2 = Calculations.DynamicViscosityWorkO2();
            Assert.AreEqual(expectedDynamicViscosityWorkO2, actualDynamicViscosityWorkO2, 0.001);


            double expectedDynamicViscosityWorkN2 = 0.00002282;
            var actualDynamicViscosityWorkN2 = Calculations.DynamicViscosityWorkN2();
            Assert.AreEqual(expectedDynamicViscosityWorkN2, actualDynamicViscosityWorkN2, 0.001);


            double expectedDynamicViscosityWorkSO2 = 0.00001757;
            var actualDynamicViscosityWorkSO2 = Calculations.DynamicViscosityWorkSO2();
            Assert.AreEqual(expectedDynamicViscosityWorkSO2, actualDynamicViscosityWorkSO2, 0.001);


            double expectedDynamicViscosityWorkNO2 = 0.00004035;
            var actualDynamicViscosityWorkNO2 = Calculations.DynamicViscosityWorkNO2();
            Assert.AreEqual(expectedDynamicViscosityWorkNO2, actualDynamicViscosityWorkNO2, 0.001);


            double expectedDynamicViscosityWorkH2S = 0.00001781;
            var actualDynamicViscosityWorkH2S = Calculations.DynamicViscosityWorkH2S();
            Assert.AreEqual(expectedDynamicViscosityWorkH2S, actualDynamicViscosityWorkH2S, 0.001);


            double expectedDynamicViscosityWorkH2O = 0.00001623;
            var actualDynamicViscosityWorkH2O = Calculations.DynamicViscosityWorkH2O();
            Assert.AreEqual(expectedDynamicViscosityWorkH2O, actualDynamicViscosityWorkH2O, 0.001);


            double expectedDynamicViscosityGasWork = 0.00001451;
            var actualDynamicViscosityGasWork = Calculations.DynamicViscosityGasWork();
            Assert.AreEqual(expectedDynamicViscosityGasWork, actualDynamicViscosityGasWork, 0.001);


            double expectedDryGasVolumatricFlowWork = 12.61223735;
            var actualDryGasVolumatricFlowWork = Calculations.DryGasVolumatricFlowWork();
            Assert.AreEqual(expectedDryGasVolumatricFlowWork, actualDryGasVolumatricFlowWork, 0.001);


            double expectedCyclonesFlowArea = 3.603496386;
            var actualCyclonesFlowArea = Calculations.CyclonesFlowArea();
            Assert.AreEqual(expectedCyclonesFlowArea, actualCyclonesFlowArea, 0.001);


            double expectedInnerDiameterSingleCyclone = 824.0971697;
            var actualInnerDiameterSingleCyclone = Calculations.InnerDiameterSingleCyclone();
            Assert.AreEqual(expectedInnerDiameterSingleCyclone, actualInnerDiameterSingleCyclone, 0.001);


            double expectedCycloneGasVilosity = 3.379617834;
            var actualCycloneGasVilosity = Calculations.CycloneGasVilosity();
            Assert.AreEqual(expectedCycloneGasVilosity, actualCycloneGasVilosity, 0.001);


            double expectedDiffDisignOptimal = -3.439490446;
            var actualDiffDisignOptimal = Calculations.DiffDisignOptimal();
            Assert.AreEqual(expectedDiffDisignOptimal, actualDiffDisignOptimal, 0.001);


            double expectedHydralicResistance = 7312.158357;
            var actualHydralicResistance = Calculations.HydralicResistance();
            Assert.AreEqual(expectedHydralicResistance, actualHydralicResistance, 0.001);


            double expectedCoefficient = 5996.453771;
            var actualCoefficient = Calculations.Coefficient();
            Assert.AreEqual(expectedCoefficient, actualCoefficient, 0.001);


            double expectedMinimumDustSize = 0.000001576;
            var actualMinimumDustSize = Calculations.MinimumDustSize();
            Assert.AreEqual(expectedMinimumDustSize, actualMinimumDustSize, 0.00000001);


            double[] expectedArgIntegralDistributionFunction = new double[] { 0.569394319, 1.149280633, 1.709736764, 2.279792021, 2.859678336, 3.41030341, 3.990189724 };
            var actualArgIntegralDistributionFunction = Calculations.ArgIntegralDistributionFunction();
            for (int i = 0; i < expectedArgIntegralDistributionFunction.Length; i++)
            {
                Assert.AreEqual(expectedArgIntegralDistributionFunction[i], actualArgIntegralDistributionFunction[i], 0.001);
            }


            double[] expectedIntDistributionFunctionAll = new double[] { 0.4245, 0.7457, 0.8385, 0.9109, 0.999995814, 0.999999961, 0.999995916 };
            var actualIntDistributionFunctionAll = Calculations.IntDistributionFunctionAll();
            for (int i = 0; i < expectedIntDistributionFunctionAll.Length; i++)
            {
                Assert.AreEqual(expectedIntDistributionFunctionAll[i], actualIntDistributionFunctionAll[i], 0.001);
            }


            double[] expectedFractionalDegreeGasPurificationFirst = new double[] { 71.225, 87.285, 91.925, 95.545, 99.95575734, 99.99607301, 99.99979582 };
            var actualFractionalDegreeGasPurificationFirst = Calculations.FractionalDegreeGasPurificationFirst();
            for (int i = 0; i < expectedFractionalDegreeGasPurificationFirst.Length; i++)
            {
                Assert.AreEqual(expectedFractionalDegreeGasPurificationFirst[i], actualFractionalDegreeGasPurificationFirst[i], 0.001);
            }


            double[] expectedProcentageDustParticles = new double[] { 1.5, 1.5, 4, 7, 14, 22, 50 };
            var actualProcentageDustParticles = Calculations.ProcentageDustParticles();
            for (int i = 0; i < expectedProcentageDustParticles.Length; i++)
            {
                Assert.AreEqual(expectedProcentageDustParticles[i], actualProcentageDustParticles[i], 0.001);
            }


            double expectedGeneralGasPurificationFirst = 98.73564;
            var actualGeneralGasPurificationFirst = Calculations.GeneralGasPurificationFirst();
            Assert.AreEqual(expectedGeneralGasPurificationFirst, actualGeneralGasPurificationFirst, 0.001);


            double[] expectedResidualContentDustFractionsGasFirst = new double[] { 0.17265, 0.07629, 0.1292, 0.1247400000, 0.0024775891, 0.0003455752, 0.0000408352 };
            var actualResidualContentDustFractionsGasFirst = Calculations.ResidualContentDustFractionsGasFirst();
            for (int i = 0; i < expectedResidualContentDustFractionsGasFirst.Length; i++)
            {
                Assert.AreEqual(expectedResidualContentDustFractionsGasFirst[i], actualResidualContentDustFractionsGasFirst[i], 0.001);
            }


            double expectedResidualDustContentGasFirst = 0.505744;
            var actualResidualDustContentGasFirst = Calculations.ResidualDustContentGasFirst();
            Assert.AreEqual(expectedResidualDustContentGasFirst, actualResidualDustContentGasFirst, 0.001);
        }
    }    
}